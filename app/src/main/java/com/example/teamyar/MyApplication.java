package com.example.teamyar;

import android.app.Application;

import com.example.teamyar.di.components.AppComponent;
import com.example.teamyar.di.components.DaggerAppComponent;

public class MyApplication extends Application {

    AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        appComponent= DaggerAppComponent.builder().build();

    }

   public AppComponent getComponent() {
        return appComponent;
   }


}
