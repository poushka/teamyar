package com.example.teamyar.main_activity;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.teamyar.models.MainResponse;
import com.example.teamyar.repos.MainRepo;

public class MainViewModel extends ViewModel {

     private MainRepo mainRepo;
     LiveData<MainResponse> mainResponse;
      MainViewModel(MainRepo mainRepo) {
          this.mainRepo = mainRepo;
          mainResponse=mainRepo.main_response;
      }
      //using two way databinding
    public String email = "" ;
    public String password = "";

    //interface to pass warning to activity if email or password is empty
    private WarningHandler warningHandler;

    public void login (View view) {
        if (email.isEmpty()) {
           warningHandler.showWarning("لطفا ایمیل خود را وارد نمایید");
        }else if (password.isEmpty()) {
            warningHandler.showWarning("لطفا رمز عبور خود را وارد نمایید");
        }else {
            mainRepo.login(email,password);
        }
    }
     void setWarningHandler(WarningHandler warningHandler) {
        this.warningHandler=warningHandler;
    }
     LiveData<Boolean> loading() {
        return mainRepo.loading;
    }

   interface WarningHandler {
        public void showWarning(String waringn);
   }

    @Override
    protected void onCleared() {
        super.onCleared();
        mainRepo.clear();
    }
}
