package com.example.teamyar.main_activity;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.teamyar.repos.MainRepo;

import javax.inject.Inject;

public class MainFactory extends ViewModelProvider.NewInstanceFactory {

    //provinding repository for viewmodel
     private MainRepo mainRepo;

     @Inject
     public MainFactory(MainRepo mainRepo) {
          this.mainRepo=mainRepo;
     }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MainViewModel(mainRepo);
    }
}
