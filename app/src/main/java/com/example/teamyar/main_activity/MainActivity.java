package com.example.teamyar.main_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import android.widget.Toast;

import com.example.teamyar.MyApplication;
import com.example.teamyar.R;

import com.example.teamyar.databinding.ActivityMainBinding;

import com.example.teamyar.panel_activity.PanelActivity;



import java.util.ArrayList;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainViewModel.WarningHandler {


    ActivityMainBinding binding ;
    MainViewModel mainViewModel ;

    @Inject
    MainFactory factory;
    ObservableBoolean loading = new ObservableBoolean();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //injecting to main activity
        ((MyApplication) getApplication()).getComponent().injectMain(this);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_main);

        mainViewModel=new ViewModelProvider(this,factory).get(MainViewModel.class);
        mainViewModel.setWarningHandler(this);

        binding.setViewmodel(mainViewModel);
        binding.setLoading(loading);

        //if the result of login is okey we navigate user to panel activity along with cookies list
        mainViewModel.mainResponse.observe(this, it -> {
            if (it.getMessage().equalsIgnoreCase("ok")) {
                Intent intent = new Intent(MainActivity.this, PanelActivity.class);
                intent.putExtra(PanelActivity.COOCKIES,new ArrayList<>(it.getCoockies()));
                startActivity(intent);
                finish();
            }else {
                //show error if there is a problem
                Toast.makeText(this, it.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        //showing loading whilie loging
        mainViewModel.loading().observe(this, it -> {
            loading.set(it);
        });



    }


    //show warning if email or password is empty
    @Override
    public void showWarning(String waringn) {
        Toast.makeText(this, waringn, Toast.LENGTH_LONG).show();
    }
}
