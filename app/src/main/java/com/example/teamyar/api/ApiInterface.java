package com.example.teamyar.api;


import com.example.teamyar.models.LoginResponse;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("login?lang_id=4&m_id=13")
    Single<Response<LoginResponse>> login(@Field("login") String login , @Field("password") String password);


}
