package com.example.teamyar.panel_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;

import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.teamyar.R;
import com.example.teamyar.databinding.ActivityPanelBinding;


import java.util.ArrayList;
import java.util.HashSet;

public class PanelActivity extends AppCompatActivity {

    //List Of Cookies
    public final static String COOCKIES = "coockies";
    ArrayList<String> cookies = new ArrayList<>();


    ObservableBoolean loading = new ObservableBoolean(true);
    ActivityPanelBinding binding ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_panel);
        binding.setLoading(loading);
        cookies=getIntent().getStringArrayListExtra(COOCKIES);
        //add recived cockies from login reposone to webview
        HashSet<String> cookies_hash = new HashSet<>(cookies);
        CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(binding.webview.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.removeSessionCookie();
        for (String cookie : cookies_hash) {
            cookieManager.setCookie("https://jobs.teamyar.com",cookie);
        }
        cookieSyncManager.sync();
        binding.webview.getSettings().setJavaScriptEnabled(true);
        binding.webview.getSettings().setDomStorageEnabled(true);

        //Show loading before Page is fully loaded
        binding.webview.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                loading.set(false);
            }


        });

        binding.webview.loadUrl("https://jobs.teamyar.com/?page=/home/index");
    }
}
