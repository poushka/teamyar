package com.example.teamyar.models;

import java.util.List;

public class MainResponse {
    List<String> coockies;
    String message ;

    public MainResponse(List<String> coockies, String message) {
        this.coockies = coockies;
        this.message = message;
    }

    public List<String> getCoockies() {
        return coockies;
    }

    public void setCoockies(List<String> coockies) {
        this.coockies = coockies;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
