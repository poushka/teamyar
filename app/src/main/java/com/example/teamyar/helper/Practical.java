package com.example.teamyar.helper;

import android.util.Log;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class Practical {

    //parse retrofit failure

    public static String noInternetParser(Throwable t) {
        if (t instanceof SocketTimeoutException) {
           return "عدم موفقیت در ارتباط با سرور";
        }  else  if (t instanceof IOException) {
            return "ارتباط شما با اینترنت قطع میباشد ";
        }
        else {
            return "خطا در ارتباط با سرور";
        }
    }
}
