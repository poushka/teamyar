package com.example.teamyar.di.components;

import com.example.teamyar.di.moudles.NetworkModule;
import com.example.teamyar.main_activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface AppComponent {
    public void injectMain(MainActivity mainActivity);
}
