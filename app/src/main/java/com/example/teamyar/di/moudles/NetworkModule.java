package com.example.teamyar.di.moudles;


import com.example.teamyar.api.ApiInterface;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


//providing all required network components
@Module
public class NetworkModule {

    @Provides
    @Singleton
    public GsonConverterFactory gson() {
         return GsonConverterFactory.create();
    }
    @Provides
    @Singleton
    public OkHttpClient okHttpClient() {
        return  new OkHttpClient().newBuilder().readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30,TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    public RxJava2CallAdapterFactory rxFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    public Retrofit getRetorift(OkHttpClient okHttpClient,GsonConverterFactory gsonFacory,RxJava2CallAdapterFactory
                                 rxAdapter) {
        return new Retrofit.Builder()
                .baseUrl("https://jobs.teamyar.com/public/portal/")
                .client(okHttpClient)
                .addConverterFactory(gsonFacory)
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    @Provides
    @Singleton
    public ApiInterface apiInterface(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }



}



