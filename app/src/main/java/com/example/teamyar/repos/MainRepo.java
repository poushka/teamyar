package com.example.teamyar.repos;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.teamyar.api.ApiInterface;
import com.example.teamyar.helper.Practical;
import com.example.teamyar.models.LoginResponse;
import com.example.teamyar.models.MainResponse;
import com.google.gson.Gson;

import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class MainRepo {

    private ApiInterface apiInterface;
    private CompositeDisposable compositeDisposable;

    @Inject
    public MainRepo(ApiInterface apiInterface) {
     this.apiInterface=apiInterface;
     compositeDisposable=new CompositeDisposable();
    }

    private MutableLiveData<Boolean> loading_ = new MutableLiveData<>();
    public LiveData<Boolean> loading = loading_;


    private MutableLiveData<MainResponse> main_response_ = new MutableLiveData<>();
    public LiveData<MainResponse> main_response = main_response_;


    //request for login
    public LiveData<MainResponse> login(String login , String password) {
        loading_.postValue(true);
        compositeDisposable.add(apiInterface.login(login,password)
             .subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribeWith(new DisposableSingleObserver<Response<LoginResponse>>() {
                 @Override
                 public void onSuccess(Response<LoginResponse> response) {
                     //sending result to Mainactivity using livedata
                     loading_.postValue(false);
                     List<String> cookies = response.headers().values("Set-Cookie");
                     main_response_.postValue(new MainResponse(cookies,response.body().getMessage()));
                 }

                 @Override
                 public void onError(Throwable e) {
                     loading_.postValue(false);
                     String failureMessage = Practical.noInternetParser(e);
                     main_response_.postValue(new MainResponse(null,failureMessage));

                 }
             }));
        return  main_response;
    }

    public void clear() {
        compositeDisposable.dispose();
    }
}
